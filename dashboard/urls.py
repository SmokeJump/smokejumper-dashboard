from django.urls import path

from . import views

app_name = 'dashboard'

urlpatterns = [
    # /dashboard/
    path('', views.index, name='index'),

    # /dashboard/login
    path('login', views.logIn, name='log_in'),

    #/dashboard/logout
    path('logout', views.logOut, name='log_out'),

    #ajax calls
    path('get_all_users', views.get_all_users, name='get_all_users'),
    path('fire_coordinates', views.fire_coordinates_for_ajax, name='fire_coordinates_for_ajax')

]
