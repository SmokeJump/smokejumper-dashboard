from django import forms
from django.forms import ModelForm
from .models import Users

class LoginForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'placehoder':'username'}))
    email = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Email Address'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password'}))
    class Meta:
        model = Users
        fields = ['username', 'email', 'password']
        
