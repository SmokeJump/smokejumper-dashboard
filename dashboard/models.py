from django.db import models

# Create your models here.
class Users(models.Model):
    type_of_respondant = models.CharField(max_length=30)
    name = models.CharField(max_length=30)
    #last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=100)
    firebase_id = models.CharField(max_length=100)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)
    highlight = models.BooleanField(default=False)
    heart_rate = models.IntegerField(default=0)
    carbon_monoxide_level = models.IntegerField(default=0)
    sorrounding_temperature = models.IntegerField(default=0)
    unique_id = models.AutoField(primary_key=True)
    def __str__(self):
        return self.name
