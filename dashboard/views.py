from django.shortcuts import render, redirect
import plotly.plotly as py
import pandas as pd
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from .forms import LoginForm
import pyrebase
import geopy.distance as geo_d
from .models import Users
import numpy as np
from django.http import JsonResponse

#These are urls for other pages on the website
INDEX_URL = "/dashboard/"
LOGIN_URL = "/dashboard/login"

config = {
    'apiKey': "AIzaSyCmpmXBztg8rRdUINXA08sRe3tEcCdaBVU",
    'authDomain': "smokejumper-6adc2.firebaseapp.com",
    'databaseURL': "https://smokejumper-6adc2.firebaseio.com",
    'projectId': "smokejumper-6adc2",
    'storageBucket': "smokejumper-6adc2.appspot.com",
    'messagingSenderId': "650905800259"
}

#firebase.initializeApp(config)
firebase = pyrebase.initialize_app(config)

# Get a reference to the database service
db = firebase.database()

# global user variable
current_user = ""

first_respondant_list = list()
fire_dict = dict()
all_respondant_types = list()

#colors
resp_colors = {
    'firefighters':'rgb(200,100,0)',
    'medic':'rgb(76,117,230)'
}
resp_colors_hightlighted = {
    'firefighters':'rgb(255,165,0)',
    'medic':'rgb(255,100,0)'
}
#######TEST########
# data to save
# data = {
#     "name": "Mortimer 'Morty' Smith"
# }
# Pass the user's idToken to the push method
#results = db.child("users").push(data, user['idToken'])

def checkSession(request):
    template = "login.html"
    print("\tChecking session")
    current_user = request.session.get('current_user', '')
    print(current_user)
    if (not current_user):
        print("\tCurrent user not registered")
        return False
    return True

def logIn(request):
    print("\tlogin function--------------")
    login_form = LoginForm
    template = "login.html"
    context = dict()
    current_user = request.session.get('current_user', '')
    context['current_user'] = current_user
    if(not current_user):
        print("\tcurrent_user not logged in")
        if request.method == 'GET':
            print("\tGet method")
            context['form'] = login_form(None)
            return render(request, template, context)
        else:
            print("\tPost")
            print("\tPassword is: ", request.POST.get('password'))
            context['form'] = login_form(request.POST),
            form = login_form(request.POST)
            print(form)
            #check if the form was filled correctly then proceed

            #don't know y this isn't working
            #if form.is_valid():
            print("\tFFFFFFFFFFform is valid")
            email = request.POST.get('email')
            password = request.POST.get('password')
            # Get a reference to the auth service
            auth = firebase.auth()
            # Log the user in
            user = auth.sign_in_with_email_and_password(email, password)
            if(not user):
                print("\tLogin didn't work.")
                return redirect(LOGIN_URL)
            else:
                request.session['current_user'] = user
                print("\tsending to index")
                return redirect(INDEX_URL)
    else:
        context = {
            'current_user':current_user
        }
        return render(request,template,context)

    return render(request, template, context)

#logout of a session
def logOut(request):
    print("\tlogging out")
    if(request.session['current_user']):
        del request.session['current_user']
    return redirect(LOGIN_URL)

# Create your views here.
def index(request):
    print("\tin index -------------------------")
    if(not checkSession(request)):
        return redirect(LOGIN_URL)
    # TODO: lattitude, longitude to be changed with center of coordinates acquired from get_fire_position()
    initialize_first_respondants_from_firebase()

    not_highlighted_users = Users.objects.filter(highlight=False)
    highlighted_users = Users.objects.filter(highlight=True)
    fire_lat_long = get_location_points_for_fire()

    p = draw_mapbpx_map(not_highlighted_users, highlighted_users, fire_lat_long)

    context = {
        'map' : str(p)
    }

    print("I at home")
    return render(request, "map_section.html", context)

#this method returns a plot object for the plotly mapbox
def draw_mapbpx_map(not_highlighted_users, highlighted_users, fire_lat_long):
    global resp_colors, resp_colors_hightlighted
    data = list()
    for respondant_type in all_respondant_types:
        temp = not_highlighted_users.filter(type_of_respondant = respondant_type)
        r_color = resp_colors[respondant_type]
        size = 20
        data = make_data_block_for_scattermapbox(data, temp, r_color, size)

    if highlighted_users.exists():
        for respondant_type in all_respondant_types:
            temp = highlighted_users.filter(type_of_respondant = respondant_type)
            r_color = resp_colors_hightlighted[respondant_type]
            size = 22
            data = make_data_block_for_scattermapbox(data, temp, r_color, size)

    fire_area_source = get_fire_area_pollygon_dict(fire_lat_long)
    fill_fire_area = {
        "id": "park-boundary",
        "type": "fill",
        "source": fire_area_source,
        "paint": {
            "fill-color": "#E63E2C",
            "fill-opacity": 0.4
        },
        "filter": ["==", "$type", "Polygon"]
    }

    layers=[
        dict(
            sourcetype = 'geojson',
            source =fire_area_source,
            color='rgb(0,0,230)',
            type = 'line',
            line=dict(width=1.5),
       ),
       dict(
            sourcetype = 'geojson',
            source = fire_area_source,
            type='fill',
            opacity = 0.4,
            color = "#888888"
       )

     ]

    latitude = 37.3382
    longitude = -121.8863
    zoom_var = 14

    layout = go.Layout(
        autosize=True,
        hovermode='closest',
        mapbox=dict(
            accesstoken='pk.eyJ1IjoiYWtzaGF0NDYiLCJhIjoiY2ptbnY5N2RnMHh4MzNwbnkyaDQ0Nmx6dCJ9.O1wq_YvpFSvvxUsun8-_EQ',
            layers=layers,
            style='mapbox://styles/akshat46/cjp7tz1tx3uxv2sqzu8eko8wz',
            bearing=0,
            center=dict(
                lat=latitude,
                lon=longitude
            ),
            pitch=0,
            zoom=zoom_var
        )
    )

    fig = dict(data=data, layout=layout)
    return plot(fig, include_plotlyjs=False, output_type='div',  config={'displayModeBar': False,'showLink': False})

#This method gives the blueprint or the map of where to draw the fire pollygon
def get_fire_area_pollygon_dict(lat_long):
    #coordinates = [lon, lat] -> this is the format
    i = 0
    lat_lon_len = len(lat_long[0])
    coord = []
    temp = []
    while i < lat_lon_len:
        temp = [lat_long[1][i], lat_long[0][i]]
        coord.append(temp)
        i = i + 1

    temp = [lat_long[1][0], lat_long[0][0]]
    coord.append(temp)

    fire_polly_dict = {
        "type": "FeatureCollection",
        "features": [{
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    coord
                ]
            }
        }]
    }
    return fire_polly_dict

def initialize_first_respondants_from_firebase():
    global db, all_respondant_types
    db_instance = db.get()

    #get the values from the database instance
    all_the_tables = db_instance.val()

    #get each of the nodes of the database on firebase from the returned object above
    # This is the outermost layer of the table
    table_keys = list(all_the_tables.keys())
    all_respondant_types = table_keys[1:]
    #skip the first element of the list
    for type_of_object in table_keys[1:]:
        #Go through each of the main tables other than the first one
        ind_object_table = all_the_tables[type_of_object] #is a touple
        #iterate through the dict and get the latlong from the dictionary objects

        #Keys for the inner sub layer, like the firefighter objects or the medic objects in the data
        ind_object_table_keys = list(ind_object_table.keys())
        for ind_object_key in ind_object_table_keys:
            #These are the keys for the attributes of the individual objects
            object_attr_keys = list(ind_object_table[ind_object_key].keys())
            fireb_id = ind_object_key
            #local_object = make_object_locally(type_of_object, field_keys_of_object, ind_object)
            c_level = ind_object_table[ind_object_key][object_attr_keys[0]]
            h_rate = ind_object_table[ind_object_key][object_attr_keys[1]]
            lat_long = ind_object_table[ind_object_key][object_attr_keys[2]].split(',')
            n = ind_object_table[ind_object_key][object_attr_keys[3]]
            s_temp = ind_object_table[ind_object_key][object_attr_keys[4]]
            obj, create = Users.objects.get_or_create(firebase_id=fireb_id)
            if create:
                print("-> Had to create this one", n)
            obj.name = n
            obj.latitude = float(lat_long[0])
            obj.longitude = float(lat_long[1])
            obj.type_of_respondant = type_of_object
            obj.heart_rate = h_rate
            obj.carbon_monoxide_level = c_level
            obj.sorrounding_temperature = s_temp
            obj.save()
            print("---------------->", obj.type_of_respondant)
            print(obj.name)

    return

def get_location_of_respondants(respondants):
    lat = list(respondants.values_list('latitude', flat=True))
    lon = list(respondants.values_list('longitude', flat=True))
    lat = pd.Series(lat)
    lon = pd.Series(lon)
    #combine them together to return the lat lon array
    lat_long = []
    lat_long.append(lat)
    lat_long.append(lon)
    return lat_long

#This method gets the location points for the fire from firebase
def get_location_points_for_fire():
    global fire_dict, db
    data_ref = db.get()
    #get the values of all the tables names
    data_ref_values = data_ref.val()
    #get the list of all the tables names
    data_ref_keys = list(data_ref.val())
    #we only want the first one, since thats the only fire one
    fire_objs =  data_ref_values[data_ref_keys[0]]
    #get all the keys of the different fires
    fire_keys = list(fire_objs.keys())
    #get the individual fire ojects and store them in the dict
    for fire_k in fire_keys:
        fire_dict[fire_k] = fire_objs[fire_k]
    print(fire_dict)

    #just returning the first object right not
    #	----->need to change this later
    np_array = np.array(fire_dict[fire_keys[0]])
    lat = np_array[:,0].tolist()
    lon = np_array[:,1].tolist()

    lat_lon = list()
    lat_lon.append(list(lat))
    lat_lon.append(list(lon))
    print("\t", lat_lon)
    return lat_lon

def make_data_block_for_scattermapbox(data, respondants, r_color, s):
    names = list(respondants.values_list('name', flat=True))
    lat_long = get_location_of_respondants(respondants)
    lat_long = get_location_of_respondants(respondants)

    go_map = go.Scattermapbox(
            lat=lat_long[0],
            lon=lat_long[1],
            mode='markers',
            marker=dict(
                size=s,
                color=r_color
            ),
            text=names
        )
    data.append(go_map)
    return data

#Ajax files##############################################
def get_all_users(request):
    test=  request.GET.get('test',None)
    print("AJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAx", test)
    global all_respondant_types
    prefix = ['highlighted_', 'non_highlighted_']
    data = dict()
    users_list = list()
    # for respondant_type in all_respondant_types:
    #     users = Users.objects.filter(type_of_respondant = respondant_type, highlight = False)
    #     data[prefix[0]+respondant_type] = users
    # for respondant_type in all_respondant_types:
    #     users = Users.objects.filter(type_of_respondant = respondant_type, highlight = True)
    #     data[prefix[1]+respondant_type] = users

    users = Users.objects.all()
    for u in users:
        temp = dict()
        temp['id'] = u.unique_id
        temp['firebase_id'] = u.firebase_id
        temp['name'] = u.name
        temp['coordinates'] = [u.longitude, u.latitude] # (longitude, latitude) because thats the format used by mapboxgl
        temp['heart_rate'] = u.heart_rate
        temp['surrounding_temperature'] = u.sorrounding_temperature
        temp['carbon_monoxide_level'] = u.carbon_monoxide_level
        users_list.append(temp)

    # data['type_of_respondant'] = all_respondant_types
    # data['prefixes'] = prefix

    data={
        'users':users_list
    }
    return JsonResponse(data)

#fire coordinates for AJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAx
def fire_coordinates_for_ajax(request):
    lat_lon = get_location_points_for_fire()
    print("\t", lat_lon)
    data = {
        'lat_lon':lat_lon
    }
    return JsonResponse(data)

#get all the highlighted users
def get_all_highlighted_users(request):
    global all_respondant_types
    data = dict()
    for respondant_type in all_respondant_types:
        users = Users.objects.filter(type_of_respondant = respondant_type, highlight = True)
        data[respondant_type] = users
    data['type_of_respondant'] = all_respondant_types
    return JsonResponse(data)

#this method returns the straight line distance between two points
def distance_between_two_pts(request):
    cord1 = request.GET.get('coord1', None)
    cord1 = request.GET.get('coord2', None)

    data = {
        'distance': geo_d.distance(coord1, coord2).mi
    }
    #returns in miles by default
    return JsonResponse(data)
