feather.replace()
var open = [];

$('.ui.accordion')
.accordion()
;

function openBar(){
	$(".rightbar").height(setHeight());
	$(".rightbar").removeClass("hover-on");
	$(".rightbar").addClass("hover-off");
	$(".sidebar-element").fadeIn(100);
	$(".sidebar-element-wrapper").fadeIn(800).delay(800);
	$(".rightbar .sidebar-close-button").fadeIn("1000");
	// document.querySelector('.open-button').style.display = "none";
}

function closeBar(){
	$(".sidebar-element").fadeOut(100);
	$(".sidebar-element-wrapper").fadeOut(100).delay(400);
	$(".rightbar").height(75).delay(800);
	$(".rightbar").removeClass("hover-off");
	$(".rightbar").addClass("hover-on");
	$(".rightbar .sidebar-close-button").fadeOut("200");
}

function setHeight(){
	var height = $(window).height();
	height = height - 40;
	return height;
}

$(".label").click(function(){
	$(this).parent().parent().toggleClass("selected");
	if($(this).parent().parent().attr('id') == "get-all-highlight" || $(this).parent().parent().attr('id') == "get-all-detail" || $(this).parent().parent().attr('id') == "calculate-distance"){
		var element_type = $(this).parent().parent().attr('id');
		if(element_type=="get-all-highlight"||element_type=="get-all-detail"){
			var item = "";
			for (var i = 0; i<users.length; i++){
				item = item + '<div class="list-item" id ="' + users[i].id + '">' + users[i].name + '</div>'
				//<div id="pin-card"><i data-feather="flag" class="icon"></i></div>
			}
		}
		$('#' + element_type + ' .content').html(item);
		feather.replace();

		if(element_type == "get-all-highlight"){
			var index = open.indexOf("get-all-highlight");
			if(index>-1){
				open.splice(index, 1);
				// whatever you wanna do when the menu is closed
				$('.detail-label-selected').remove();
				$('.marker-selected').remove();
			}
			else{
				open.push("get-all-highlight");
				$('#get-all-highlight .list-item').click(function(){
					$(this).toggleClass("detail-label-selected");
					highlightClicked(this.id);
				});
			}
		}

		if(element_type == "calculate-distance"){
			$('#map').toggleClass("change-cursor");
			$('.banner-wrapper').toggleClass("show-banner-wrapper");
			setTimeout(function(){
				$('.controls-wrapper').toggleClass("show-controls");
			},200);
			$("#control:nth-child(2)").click(function(){
				calcDistanceClosed();
				calcDistanceOpened();
			});
			var index = open.indexOf("calculate-distance");
			if(index>-1){
				calcDistanceClosed();
				open.splice(index, 1);
			}
			else{
				open.push("calculate-distance");
				calcDistanceOpened(index);
			}
		}

		if(element_type == "get-all-detail"){
			var index = open.indexOf("get-all-detail");
			if(index>-1){
				open.splice(index, 1);
				$('.marker-selected').remove();
			}
			else{
				open.push("get-all-detail");
			}
			$(".sidebar-element .list-item").click(function(){
				detailClicked(this.id);
			});
		}
		console.log("open end = " + open);
	}
});

var card_colors = ['#2A9D8F', '#E9C46A', '#F4A261', '#E76F51', '#E84A5F', '#FF847C', '#FECEA8', '#99B898']

var color_index = -1;

function get_color(){
	if(color_index == card_colors.length-1){
		color_index = -1;
	}
	color_index++
	return(card_colors[color_index])
}

var highlight_items_clicked = [];
function highlightClicked(id){
	var index = highlight_items_clicked.indexOf(id);
	//remove if highlighted item clicked again
	if(index>-1){
		$("#" + id + '.marker-selected').remove();
		highlight_items_clicked.splice(index, 1);
		return;
	}
	else{
		//add element
		firefighter = users.find(x => x.id == id);
		var el = document.createElement('div');
		el.className = 'marker-selected';
		$(el).attr('id', id);
		new mapboxgl.Marker(el).setLngLat(firefighter.coordinates).addTo(map);
		highlight_items_clicked.push(id);
		return;
	}
}

var last_detail_item_clicked = -1;
function detailClicked(id){
	if(last_detail_item_clicked == -1){
		var el = document.createElement('div');
		el.className = 'detail-card';
		$(document.body).append(el);
	}
	if(id == last_detail_item_clicked){
		$('.detail-card').remove();
		last_detail_item_clicked = -1;
		return;
	}
	else{
		$('.detail-card').empty();
	}
	firefighter = users.find(x => x.id == id);
	$('.detail-card').append('<div id="detail-close"><i data-feather="x"></i></div><div class="name">'+ firefighter.name + '</div><div class="flex-wrapper"><div class="flex-container"><div class="flex-item"><div class="detail-label"><i class ="icon" data-feather="thermometer"></i>Temperature</div><div class="detail">'+ firefighter.surrounding_temperature +'<span class="detail-unit">°F</span></div></div><div class="flex-item"><div class="detail-label"><i class ="icon" data-feather="heart"></i> Heart Rate</div><div class="detail">'+ firefighter.heart_rate + '<span class="detail-unit">BPM</span></div></div><div class="flex-item"><div class="detail-label"><i class ="icon" data-feather="cloud"></i> CO Levels</div><div class="detail">' + firefighter.carbon_monoxide_level + '<span class="detail-unit">PPM</span></div></div></div></div>');
	feather.replace()
	$('.detail-card').css('background-color', get_color());
	last_detail_item_clicked = id;
	$('#detail-close').click(function(){
		last_detail_item_clicked = -1;
		$(this).parent().remove();
	});
}

function calcDistanceOpened(){
	distanceDrawer();
}

function calcDistanceClosed(){
	removeDistanceDrawer();
}
