var users = [];
var fire_coordinates = [[]];

$.ajax({
  url: '/dashboard/get_all_users',
  dataType: 'json',
  success: function (data) {
    console.log("here");
    users = data.users;
    drawMarkers();
  }
});

$.ajax({
  url: '/dashboard/fire_coordinates',
  dataType: 'json',
  success: function (data) {
    console.log("here in fire initialization");
    fire_coordinates = data.lat_lon;
    drawFirePollygon();
  }
});
