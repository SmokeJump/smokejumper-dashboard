function openNav(){
	$(".sidenav").width(300);
	$(".sidenav").height(600);
	$(".nav-element").fadeIn();
	$(".sidenav .close-button").fadeIn("1000");
	document.querySelector('.open-button').style.display = "none";
}

function closeNav(){
	$(".sidenav").width(75);
	$(".sidenav").height(75);
	$(".nav-element").fadeOut();
	$(".sidenav .close-button").fadeOut("200");
	$(".open-button").fadeIn("1000");
}
