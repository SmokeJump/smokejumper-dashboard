function clearContainer() {
    var element = document.getElementById('container');
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

function showMap() {
    console.log('MAP shown');
    // clearContainer();
    // var element = document.createElement('div');
    // var textnode = document.createTextNode('MAP');
    // element.appendChild(textnode);
    // document.getElementById('container').appendChild(element);
}

function toggleMenu() {
    document.getElementById('sidebar').classList.toggle('active');
}

window.onload = function() {
    var lis = document.getElementById('sidebar').getElementsByTagName('li');
    console.log("On load ", lis);
    console.log("a ", lis[0].getElementsByTagName('a')[0]);
    lis[0].getElementsByTagName('a')[0].classList.toggle('focus');
};
