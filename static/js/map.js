mapboxgl.accessToken = 'pk.eyJ1IjoiYWtzaGF0NDYiLCJhIjoiY2ptbnY5N2RnMHh4MzNwbnkyaDQ0Nmx6dCJ9.O1wq_YvpFSvvxUsun8-_EQ';

var map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/light-v9',
  center: [-121.8863, 37.3382],
  zoom: 12
});

function drawMarkers(){
  console.log("here2 " + users[0].coordinates);
  users.forEach(function(user) {
    var el = document.createElement('div');
    el.className = 'marker';
    el.setAttribute("id", user.id);
    new mapboxgl.Marker(el).setLngLat(user.coordinates).addTo(map);
  });
  $('.marker').click(function(){
    $(this).css('background-image','url("{% static "images/firefighter-selected.svg" %}"');
    console.log("clicked");
  });
}
