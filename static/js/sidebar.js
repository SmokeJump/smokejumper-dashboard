feather.replace()
var open = false;

$('.ui.accordion')
.accordion()
;

function openBar(){
	$(".rightbar").height(setHeight());
	$(".rightbar").removeClass("hover-on");
	$(".rightbar").addClass("hover-off");
	$(".sidebar-element").fadeIn(100);
	$(".sidebar-element-wrapper").fadeIn(800).delay(800);
	$(".rightbar .sidebar-close-button").fadeIn("1000");
	// document.querySelector('.open-button').style.display = "none";
}

function closeBar(){
	$(".sidebar-element").fadeOut(100);
	$(".sidebar-element-wrapper").fadeOut(100).delay(400);
	$(".rightbar").height(75).delay(800);
	$(".rightbar").removeClass("hover-off");
	$(".rightbar").addClass("hover-on");
	$(".rightbar .sidebar-close-button").fadeOut("200");
}

function setHeight(){
	var height = $(window).height();
	height = height - 40;
	return height;
}

$(".label").click(function(){
	$(this).parent().parent().toggleClass("selected");
	if($(this).parent().parent().attr('id') == "get-all-highlight" || $(this).parent().parent().attr('id') == "get-all-detail"){
		var element_type = $(this).parent().parent().attr('id');
		if(!open){
			var item = "";
			for (var i = 0; i<users.length; i++){
				item = item + '<div class="list-item" id ="' + users[i].id + '">' + users[i].name + '</div>'
				//<div id="pin-card"><i data-feather="flag" class="icon"></i></div>
			}
			$('#' + element_type + ' .content').html(item);
			feather.replace();
			open = true;
			if(element_type == "get-all-detail"){
				$(".sidebar-element .list-item").click(function(){
					elementClicked(this.id);
				});
			}
		}
		else{
			open = false;
		}
	}
});

var open = false;

var card_colors = ['#2A9D8F', '#E9C46A', '#F4A261', '#E76F51', '#E84A5F', '#FF847C', '#FECEA8', '#99B898']

var color_index = -1;

function get_color(){
	if(color_index == card_colors.length-1){
		color_index = -1;
	}
	color_index++
	return(card_colors[color_index])
}

var last_sidebar_item_clicked = -1;

function elementClicked(id){
	if(last_sidebar_item_clicked == -1){
		var el = document.createElement('div');
		el.className = 'detail-card';
		$(document.body).append(el);
	}
	if(id == last_sidebar_item_clicked){
		$('.detail-card').remove();
		last_sidebar_item_clicked = -1;
		return;
	}
	else{
		$('.detail-card').empty();
	}
	firefighter = users.find(x => x.id == id);
	$('.detail-card').append('<div id="detail-close"><i data-feather="x"></i></div><div class="name">'+ firefighter.name + '</div><div class="flex-wrapper"><div class="flex-container"><div class="flex-item"><div class="detail-label"><i class ="icon" data-feather="thermometer"></i>Temperature</div><div class="detail">'+ firefighter.surrounding_temperature +'<span class="detail-unit">°F</span></div></div><div class="flex-item"><div class="detail-label"><i class ="icon" data-feather="heart"></i> Heart Rate</div><div class="detail">'+ firefighter.heart_rate + '<span class="detail-unit">BPM</span></div></div><div class="flex-item"><div class="detail-label"><i class ="icon" data-feather="cloud"></i> CO Levels</div><div class="detail">' + firefighter.carbon_monoxide_level + '<span class="detail-unit">PPM</span></div></div></div></div>');
	feather.replace()
	$('.detail-card').css('background-color', get_color());
	last_sidebar_item_clicked = id;
	$('#detail-close').click(function(){
		last_sidebar_item_clicked = -1;
		$(this).parent().remove();
	});
	console.log(last_sidebar_item_clicked);
}
